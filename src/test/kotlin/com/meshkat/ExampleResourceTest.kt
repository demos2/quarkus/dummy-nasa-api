package com.meshkat

import io.quarkus.test.junit.QuarkusTest
import io.restassured.RestAssured
import org.hamcrest.CoreMatchers
import org.junit.jupiter.api.Disabled
import org.junit.jupiter.api.Test

@QuarkusTest
open class ExampleResourceTest() {
    @Test
    @Disabled
    open fun testHelloEndpoint() {
        RestAssured.given().queryParam("earth_date", "2015-1-1").queryParam("api_key", "2015-1-1").log().all()
                .`when`()["/mars-photos/api/v1/rovers/curiosity/photos/"]
                .then()
                .statusCode(200)
                .body(CoreMatchers.`is`(CoreMatchers.notNullValue()))
    }
}
