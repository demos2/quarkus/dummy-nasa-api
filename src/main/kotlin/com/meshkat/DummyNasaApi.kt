package com.meshkat

import com.fasterxml.jackson.databind.ObjectMapper
import org.eclipse.microprofile.metrics.MetricUnits
import org.eclipse.microprofile.metrics.annotation.Counted
import org.eclipse.microprofile.metrics.annotation.Timed
import java.io.File
import javax.ws.rs.GET
import javax.ws.rs.Path
import javax.ws.rs.Produces
import javax.ws.rs.QueryParam
import javax.ws.rs.core.MediaType
import javax.ws.rs.core.Response

@Path("/mars-photos/api/v1/rovers/curiosity/photos/")
class DummyNasaApi(private val objectMapper: ObjectMapper) {
    @GET
    @Counted(name = "performedChecks", description = "How many primality checks have been performed.")
    @Timed(name = "checksTimer", description = "A measure of how long it takes to perform the primality test.", unit = MetricUnits.MILLISECONDS)
    @Produces(MediaType.APPLICATION_JSON)
    fun hello(@QueryParam("earth_date") date: String?,
              @QueryParam("api_key") apiKey: String?): Response {
        val readValue = objectMapper.readValue<MarsRoverManifest>(File("dummyRoversPhotosResponse.json"), MarsRoverManifest::class.java)

        println("read value: $readValue")
        return Response
                .ok(readValue).build()
    }

}
